﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCFilmes.Controllers
{
    public class OlaMundoController : Controller
    {
        // GET: OlaMundo
        //public string Index()
        //{
        //    return "Esta é a minha action por <b>defeito</b>.";
        //}

        // GET: OlaMundo/Welcome
        //public string Welcome()
        //{
        //    return "Este é o metodo que a action welcome chama...";
        //}

        //envia com parametros
        //public string Welcome(string nome, int numVezes = 1)
        //{
        //    return HttpUtility.HtmlEncode("Olá "+ nome + ", Numero de Vezes: "+ numVezes);
        //}

        //public string Welcome(string nome, int ID = 1)
        //{
        //    return HttpUtility.HtmlEncode("Olá " + nome + ", Numero de Vezes: " + ID);
        //}

        public ActionResult Index()
        {
            return View();
        }
        //devo meter o int por defeito
        public ActionResult Welcome(string nome, int numVezes = 1)
        {
            ViewBag.Mensagem = "Ola " + nome;
            ViewBag.NumeroVezes = numVezes;

            return View();
        }
    }
}