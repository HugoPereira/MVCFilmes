﻿using MVCFilmes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MVCFilmes.Controllers
{
    public class GenerosController : Controller
    {
        //ligacao ao datacontext
        DataClasses1DataContext db = new DataClasses1DataContext();

        // GET: Generos
        public ActionResult Index()
        {
            //retornamos os generos da datacontext
            return View(db.Generos);
        }


        // GET: Generos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Generos/Create
        [HttpPost]
        public ActionResult Create(Genero novoGenero)
        {
            if (ModelState.IsValid)
            {
                db.Generos.InsertOnSubmit(novoGenero);
            }

            try
            {
                db.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Generos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Genero genero = db.Generos.FirstOrDefault(g => g.IdGenero == id);

            //caso o id nao exista
            if (genero == null)
            {
                return HttpNotFound();
            }

            return View(genero);
        }

        // POST: Generos/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Genero updateGenero)
        {
            //verifica se existe
            Genero generoAlterado = db.Generos.FirstOrDefault(g => g.IdGenero == id);

            //caso nao exista
            if (generoAlterado == null)
            {
                return HttpNotFound();
            }


            if (ModelState.IsValid)
            {
                //faz a alteracao
                generoAlterado.Descricao = updateGenero.Descricao;
            }

            try
            {
                db.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //a ? em frente ao int torna o id opcional, caso tenha ou nao id entra sempre nesta action

        // GET: Generos/Delete/5
        public ActionResult Delete(int? id)
        {
            

            //Genero genero = db.Generos.First(g => g.IdGenero == id);

            //if (genero != null)
            //{
            //    db.Generos.DeleteOnSubmit(genero);

            //    try
            //    {
            //        db.SubmitChanges();
            //    }
            //    catch (Exception e)
            //    {
            //        return View();
            //    }
            //}

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Genero genero = db.Generos.FirstOrDefault(g => g.IdGenero == id);

            //caso o id nao exista
            if (genero == null)
            {
                return HttpNotFound();
            }

            return View(genero);
        }

        // POST: Generos/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            Genero genero = db.Generos.FirstOrDefault(g => g.IdGenero == id);

            if (genero == null)
            {
                return HttpNotFound();
            }

            db.Generos.DeleteOnSubmit(genero);

            try
            {
                db.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
