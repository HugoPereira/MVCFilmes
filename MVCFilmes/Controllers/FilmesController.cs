﻿using MVCFilmes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace MVCFilmes.Controllers
{
    public class FilmesController : Controller
    {
        DataClasses1DataContext db = new DataClasses1DataContext();

        // GET: Filmes
        //public ActionResult Index(string Id)
        //{
        //    string procuraFilme = Id;
        //    //para a caixa de pesquisa
        //    //seleciona todos os filmes
        //    var filmes = from f in db.Filmes select f;

        //    //caso a string nao esteja vazia
        //    if (!string.IsNullOrEmpty(procuraFilme))
        //    {
        //        //manda o filme em que o titulo contenha a pesquisa
        //        filmes = filmes.Where(s => s.Titulo.Contains(procuraFilme));
        //    }

        //    return View(filmes);
        //}


        public ActionResult Index(string procuraFilme, string procuraGenero)
        {
            var ListaDeGeneros = new List<string>();

            //vou buscar todos os generos
            var QueryGeneros = from g in db.Generos orderby g.Descricao select g.Descricao;

            //adiciona a lista os generos existentes
            ListaDeGeneros.AddRange(QueryGeneros.Distinct());

            //viewbag e igual a uma lista de generos
            ViewBag.procuraGenero = new SelectList(ListaDeGeneros);

            //caso quisesse passar com um valor selecionado
            //ViewBag.procuraGenero = new SelectList(ListaDeGeneros, "Comédia");


            //para a caixa de pesquisa
            //seleciona todos os filmes
            var filmes = from f in db.Filmes select f;

            //caso a string nao esteja vazia
            if (!string.IsNullOrEmpty(procuraFilme))
            {
                //manda o filme em que o titulo contenha a pesquisa
                filmes = filmes.Where(s => s.Titulo.Contains(procuraFilme));
            }

            //lista o genero que tenha um descricao igual a procura
            var genero = db.Generos.FirstOrDefault(g => g.Descricao == procuraGenero);

            if (!string.IsNullOrEmpty(procuraGenero))
            {
                //manda o filme com este genero
                filmes = filmes.Where(x => x.IdGenero == genero.IdGenero);
            }


            return View(filmes);
        }

       /* [HttpPost]
        public string Index(FormCollection fc, string procuraFilme)
        {
            return "<h3> Venho de um [HttpPost] action index: " + procuraFilme + "</h3>";
        }*/


        // GET: Filmes/Details/5
        public ActionResult Details(int? id)
        {
            //caso nao exista manda o erro
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Filme filme = db.Filmes.FirstOrDefault(f => f.IdFilme == id);

            if (filme == null)
            {
                return HttpNotFound();
            }

            //encontrar o genero do filme
            Genero genero = db.Generos.FirstOrDefault(g => g.IdGenero == filme.IdGenero);

            //se o genero existir
            if (genero != null)
            {
                //bom para passar dados, pois so se pode passar um modelo do objecto(filme neste caso)
                //passa pela viewbag.(o que nos quisermos) a descrição
                ViewBag.Genero = genero.Descricao;
            }

            return View(filme);
        }

        // GET: Filmes/Create
        public ActionResult Create()
        {
            //pela viewbag passo uma lista dos generos ordenado pela discricao, o que fica guardado na tabela é o iDGenero, mas o que aparece la é a discricao
            ViewBag.IdGenero = new SelectList(db.Generos.OrderBy(g => g.Descricao), "IdGenero", "Descricao");

            return View();
        }

        // POST: Filmes/Create
        [HttpPost]
        public ActionResult Create(Filme novoFilme)
        {
            novoFilme.IdFilme = db.Filmes.Max(f => f.IdFilme) + 1;

            if (ModelState.IsValid)
            {
                db.Filmes.InsertOnSubmit(novoFilme);
            }

            try
            {
                db.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch(Exception e)
            {
                ViewBag.Erro = e;
                return View();
            }
        }

        // GET: Filmes/Edit/5
        public ActionResult Edit(int? id)
        {
            

            ViewBag.IdGenero = new SelectList(db.Generos.OrderBy(g => g.Descricao),"IdGenero", "Descricao");
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Filme filme = db.Filmes.FirstOrDefault(f => f.IdFilme == id);

            if (filme == null)
            {
                return HttpNotFound();
            }

            return View(filme);
        }

        // POST: Filmes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Filme filme)
        {
            Filme filmeAlterado = db.Filmes.FirstOrDefault(f => f.IdFilme == id);

            if (filmeAlterado == null)
            {
                return HttpNotFound();
            }

            Genero genero = db.Generos.FirstOrDefault(g => g.IdGenero == filme.IdGenero);

            if (ModelState.IsValid && genero != null)
            {
                filmeAlterado.Titulo = filme.Titulo;
                filmeAlterado.DataEstreia = filme.DataEstreia;
                filmeAlterado.IdGenero = filme.IdGenero;
            }



            try
            {
                db.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Filmes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Filme filme = db.Filmes.FirstOrDefault(f => f.IdFilme == id);

            if (filme == null)
            {
                return HttpNotFound();
            }

            return View(filme);
        }

        // POST: Filmes/Delete/5
        [HttpPost]
        public ActionResult Delete(int id)
        {
            Filme filme = db.Filmes.FirstOrDefault(f => f.IdFilme == id);

            if (filme == null)
            {
                return HttpNotFound();
            }

            db.Filmes.DeleteOnSubmit(filme);

            try
            {
                db.SubmitChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
