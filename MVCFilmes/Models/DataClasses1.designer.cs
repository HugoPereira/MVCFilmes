﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVCFilmes.Models
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="Videoclube")]
	public partial class DataClasses1DataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    partial void InsertFilme(Filme instance);
    partial void UpdateFilme(Filme instance);
    partial void DeleteFilme(Filme instance);
    partial void InsertGenero(Genero instance);
    partial void UpdateGenero(Genero instance);
    partial void DeleteGenero(Genero instance);
    #endregion
		
		public DataClasses1DataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["VideoclubeConnectionString1"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public DataClasses1DataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<Filme> Filmes
		{
			get
			{
				return this.GetTable<Filme>();
			}
		}
		
		public System.Data.Linq.Table<Genero> Generos
		{
			get
			{
				return this.GetTable<Genero>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Filmes")]
	public partial class Filme : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _IdFilme;
		
		private string _Titulo;
		
		private System.DateTime _DataEstreia;
		
		private int _IdGenero;
		
		private EntityRef<Genero> _Genero;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdFilmeChanging(int value);
    partial void OnIdFilmeChanged();
    partial void OnTituloChanging(string value);
    partial void OnTituloChanged();
    partial void OnDataEstreiaChanging(System.DateTime value);
    partial void OnDataEstreiaChanged();
    partial void OnIdGeneroChanging(int value);
    partial void OnIdGeneroChanged();
    #endregion
		
		public Filme()
		{
			this._Genero = default(EntityRef<Genero>);
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IdFilme", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int IdFilme
		{
			get
			{
				return this._IdFilme;
			}
			set
			{
				if ((this._IdFilme != value))
				{
					this.OnIdFilmeChanging(value);
					this.SendPropertyChanging();
					this._IdFilme = value;
					this.SendPropertyChanged("IdFilme");
					this.OnIdFilmeChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Titulo", DbType="NVarChar(250) NOT NULL", CanBeNull=false)]
		public string Titulo
		{
			get
			{
				return this._Titulo;
			}
			set
			{
				if ((this._Titulo != value))
				{
					this.OnTituloChanging(value);
					this.SendPropertyChanging();
					this._Titulo = value;
					this.SendPropertyChanged("Titulo");
					this.OnTituloChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_DataEstreia", DbType="Date NOT NULL")]
		public System.DateTime DataEstreia
		{
			get
			{
				return this._DataEstreia;
			}
			set
			{
				if ((this._DataEstreia != value))
				{
					this.OnDataEstreiaChanging(value);
					this.SendPropertyChanging();
					this._DataEstreia = value;
					this.SendPropertyChanged("DataEstreia");
					this.OnDataEstreiaChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IdGenero", DbType="Int NOT NULL")]
		public int IdGenero
		{
			get
			{
				return this._IdGenero;
			}
			set
			{
				if ((this._IdGenero != value))
				{
					if (this._Genero.HasLoadedOrAssignedValue)
					{
						throw new System.Data.Linq.ForeignKeyReferenceAlreadyHasValueException();
					}
					this.OnIdGeneroChanging(value);
					this.SendPropertyChanging();
					this._IdGenero = value;
					this.SendPropertyChanged("IdGenero");
					this.OnIdGeneroChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Genero_Filme", Storage="_Genero", ThisKey="IdGenero", OtherKey="IdGenero", IsForeignKey=true)]
		public Genero Genero
		{
			get
			{
				return this._Genero.Entity;
			}
			set
			{
				Genero previousValue = this._Genero.Entity;
				if (((previousValue != value) 
							|| (this._Genero.HasLoadedOrAssignedValue == false)))
				{
					this.SendPropertyChanging();
					if ((previousValue != null))
					{
						this._Genero.Entity = null;
						previousValue.Filmes.Remove(this);
					}
					this._Genero.Entity = value;
					if ((value != null))
					{
						value.Filmes.Add(this);
						this._IdGenero = value.IdGenero;
					}
					else
					{
						this._IdGenero = default(int);
					}
					this.SendPropertyChanged("Genero");
				}
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="dbo.Generos")]
	public partial class Genero : INotifyPropertyChanging, INotifyPropertyChanged
	{
		
		private static PropertyChangingEventArgs emptyChangingEventArgs = new PropertyChangingEventArgs(String.Empty);
		
		private int _IdGenero;
		
		private string _Descricao;
		
		private EntitySet<Filme> _Filmes;
		
    #region Extensibility Method Definitions
    partial void OnLoaded();
    partial void OnValidate(System.Data.Linq.ChangeAction action);
    partial void OnCreated();
    partial void OnIdGeneroChanging(int value);
    partial void OnIdGeneroChanged();
    partial void OnDescricaoChanging(string value);
    partial void OnDescricaoChanged();
    #endregion
		
		public Genero()
		{
			this._Filmes = new EntitySet<Filme>(new Action<Filme>(this.attach_Filmes), new Action<Filme>(this.detach_Filmes));
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_IdGenero", DbType="Int NOT NULL", IsPrimaryKey=true)]
		public int IdGenero
		{
			get
			{
				return this._IdGenero;
			}
			set
			{
				if ((this._IdGenero != value))
				{
					this.OnIdGeneroChanging(value);
					this.SendPropertyChanging();
					this._IdGenero = value;
					this.SendPropertyChanged("IdGenero");
					this.OnIdGeneroChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Descricao", DbType="NVarChar(50) NOT NULL", CanBeNull=false)]
		public string Descricao
		{
			get
			{
				return this._Descricao;
			}
			set
			{
				if ((this._Descricao != value))
				{
					this.OnDescricaoChanging(value);
					this.SendPropertyChanging();
					this._Descricao = value;
					this.SendPropertyChanged("Descricao");
					this.OnDescricaoChanged();
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.AssociationAttribute(Name="Genero_Filme", Storage="_Filmes", ThisKey="IdGenero", OtherKey="IdGenero")]
		public EntitySet<Filme> Filmes
		{
			get
			{
				return this._Filmes;
			}
			set
			{
				this._Filmes.Assign(value);
			}
		}
		
		public event PropertyChangingEventHandler PropertyChanging;
		
		public event PropertyChangedEventHandler PropertyChanged;
		
		protected virtual void SendPropertyChanging()
		{
			if ((this.PropertyChanging != null))
			{
				this.PropertyChanging(this, emptyChangingEventArgs);
			}
		}
		
		protected virtual void SendPropertyChanged(String propertyName)
		{
			if ((this.PropertyChanged != null))
			{
				this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}
		}
		
		private void attach_Filmes(Filme entity)
		{
			this.SendPropertyChanging();
			entity.Genero = this;
		}
		
		private void detach_Filmes(Filme entity)
		{
			this.SendPropertyChanging();
			entity.Genero = null;
		}
	}
}
#pragma warning restore 1591
